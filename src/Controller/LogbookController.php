<?php

namespace App\Controller;

use App\Entity\Logbook;
use App\Form\LogbookType;
use App\Repository\LogbookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/logbook")
 */
class LogbookController extends AbstractController
{
    /**
     * @Route("/", name="logbook_index", methods={"GET"})
     */
    public function index(LogbookRepository $logbookRepository): Response
    {
        return $this->render('logbook/index.html.twig', [
            'logbooks' => $logbookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="logbook_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $logbook = new Logbook();
        $form = $this->createForm(LogbookType::class, $logbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($logbook);
            $entityManager->flush();

            return $this->redirectToRoute('logbook_index');
        }

        return $this->render('logbook/new.html.twig', [
            'logbook' => $logbook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="logbook_show", methods={"GET"})
     */
    public function show(Logbook $logbook): Response
    {
        return $this->render('logbook/show.html.twig', [
            'logbook' => $logbook,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="logbook_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Logbook $logbook): Response
    {
        $form = $this->createForm(LogbookType::class, $logbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('logbook_index', [
                'id' => $logbook->getId(),
            ]);
        }

        return $this->render('logbook/edit.html.twig', [
            'logbook' => $logbook,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="logbook_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Logbook $logbook): Response
    {
        if ($this->isCsrfTokenValid('delete'.$logbook->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($logbook);
            $entityManager->flush();
        }

        return $this->redirectToRoute('logbook_index');
    }
}
